const addUserModalBtn = document.querySelector('#add-user-btn');
const modal = document.querySelector('#add-user-modal');
const closeBtn = document.querySelector('.close');
const submitBtn = document.querySelector('#add-user');
const formTile = document.querySelector('#form-title');
const tostDescription = document.querySelector('.desc');
const errorMessageContainer = document.querySelector('.error');

const launchToast = () => {
  const toast = document.querySelector('#toast');

  toast.className = 'show';
  setTimeout(() => {
    toast.className = toast.className.replace('show', '');
  }, 5000);
};

const users = [
  {
    id: 1,
    firstName: 'Jane',
    lastName: 'Doe',
    email: 'jane@gmail.com',
    role: 'doctor',
    status: 'active'
  },
  {
    id: 2,
    firstName: 'John',
    lastName: 'Smith',
    email: 'john@gmail.com',
    role: 'accountant',
    status: 'active'
  },
  {
    id: 3,
    firstName: 'Yemi',
    lastName: 'Soft',
    email: 'yemi@gmail.com',
    role: 'admin',
    status: 'active'
  },
  {
    id: 4,
    firstName: 'Lola',
    lastName: 'Joe',
    email: 'lola@gmail.com',
    role: 'accountant',
    status: 'in-active'
  }
];

const generateUserHTML = (user, activeClassName = '') => {
  return `
  <div class="user-card ${activeClassName}">
  <div class="user-card-body">
    <h3 class="user-card-heading">user details</h3>
    <p class="user__name bg-grey">Full name: <span class="small-text">${user.firstName}-${user.lastName}</span></p>
    <p class="user__email bg-grey">Email: <span class="small-text">${user.email}</span></p>
    <p class="user__role bg-grey">Role: <span class="small-text">${user.role}</span></p>
    <p class="user__status bg-grey">Status: <span class="small-text">${user.status}</span></p>
  </div>
  <div class="user__footer">
    <button type="button" class="btn btn-danger" id="delete-btn" data-id='${user.id}'>delete</button>
    <button type="button" class="btn btn-info" id="edit-btn" data-id='${user.id}'>edit</button>
  </div>
</div>
  `;
};

const user = new User('main-content', users, generateUserHTML);

addUserModalBtn.addEventListener('click', (evt) => {
  modal.style.display = 'block';
});

const resetForm = () => {
  errorMessageContainer.textContent = '';
  document.querySelector('#firstName').value = '';
  document.querySelector('#lastName').value = '';
  document.querySelector('#email').value = '';

  submitBtn.removeAttribute('data-id');
  submitBtn.textContent = 'Create';
  formTile.textContent = 'Create new User';
  errorMessageContainer.style.display = 'none';
};

closeBtn.addEventListener('click', () => {
  modal.style.display = 'none';
  resetForm();
});

window.onclick = (event) => {
  if (event.target == modal) {
    modal.style.display = 'none';
    resetForm();
  }
};

submitBtn.addEventListener('click', (evt) => {
  evt.preventDefault();

  if (!submitBtn.getAttribute('data-id')) {
    submitBtn.textContent = 'Create';
  }

  let firstName = document.querySelector('#firstName');
  let lastName = document.querySelector('#lastName');
  let email = document.querySelector('#email');
  let status = document.querySelectorAll('[name=status]');
  let role = document.querySelectorAll('[name=role]');

  for (let currentStatus of status) {
    if (currentStatus.checked === true) {
      status = currentStatus.value;
    }
  }

  for (let currentRole of role) {
    if (currentRole.checked === true) {
      role = currentRole.value;
    }
  }

  if (
    firstName.value.trim() === '' ||
    lastName.value.trim() === '' ||
    email.value.trim() == ''
  ) {
    errorMessageContainer.textContent = 'All field are required';
    errorMessageContainer.style.display = 'block';
    return;
  }

  const id = Math.floor(Date.now() + Math.random());

  const userDetails = {
    id,
    firstName: firstName.value,
    lastName: lastName.value,
    email: email.value,
    status,
    role
  };

  let error = {};

  if (submitBtn.textContent == 'Create') {
    error = user.add(userDetails);

    if (error && error.message) {
      errorMessageContainer.textContent = `User with ${userDetails.email} already exist`;
      return;
    }
    tostDescription.textContent = 'User created successfully';
  } else {
    const userId = submitBtn.getAttribute('data-id');

    user.edit({ ...userDetails, id: userId });

    tostDescription.textContent = 'User details updated';
  }

  resetForm();

  modal.style.display = 'none';

  launchToast();
});

// show edit user modal
// populate form with existing record
document.addEventListener('click', (evt) => {
  const userId = evt.target.getAttribute('data-id');

  if (`${evt.target.getAttribute('id')}` === 'delete-btn') {
    user.delete(userId);
  } else if (`${evt.target.getAttribute('id')}` === 'edit-btn') {
    const existingUserRecord = user.find(userId);

    modal.style.display = 'block';

    let firstName = document.querySelector('#firstName');
    let lastName = document.querySelector('#lastName');
    let email = document.querySelector('#email');
    let status = document.querySelectorAll('[name=status]');
    let role = document.querySelectorAll('[name=role]');

    firstName.value = existingUserRecord.firstName;
    lastName.value = existingUserRecord.lastName;
    email.value = existingUserRecord.email;

    for (let currentStatus of status) {
      if (currentStatus.value === existingUserRecord.status) {
        currentStatus.checked = true;
      }
    }

    for (let currentRole of role) {
      if (currentRole.value === existingUserRecord.role) {
        currentRole.checked = true;
      }
    }

    submitBtn.setAttribute('data-id', existingUserRecord.id);
    submitBtn.textContent = 'update';
    formTile.textContent = 'Update user';
  }
});
