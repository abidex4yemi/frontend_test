class User {
  constructor(containerClassName, users, generateUserHTML) {
    this.container = document.querySelector(`.${containerClassName}`);
    this.users = users;
    this.generateUserHTML = generateUserHTML;

    this.render();
  }

  render(activeClassName = '') {
    let output = '';

    this.users.forEach((user) => {
      output += this.generateUserHTML(user, activeClassName);
    });

    this.container.innerHTML = output;
  }

  add(newUser) {
    const userExist = this.users.find(
      (user) => user.email.toLowerCase() === newUser.email.toLowerCase()
    );

    if (userExist) {
      const error = {
        message: 'user exist'
      };

      return error;
    }

    this.users.push(newUser);

    this.container.insertAdjacentHTML(
      'afterbegin',
      this.generateUserHTML(newUser, 'change-border-color')
    );

    return newUser;
  }

  delete(userID) {
    this.users = this.users.filter((user) => {
      if (`${user.id}` !== `${userID}`) {
        return user;
      }
    });

    this.render();
  }

  find(userID) {
    const user = this.users.find((user) => `${user.id}` === `${userID}`);

    return user;
  }

  edit(updatedDetails) {
    this.users = this.users.map((user) => {
      if (`${user.id}` === `${updatedDetails.id}`) {
        return updatedDetails;
      }

      return user;
    });

    this.render();
  }
}
